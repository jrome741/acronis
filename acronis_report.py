#!/data/data/com.termux/files/home/HOME/local/py_env_3102/bin/python

## -------------------------------------------------- ##
## IMPORT SECTION                                     ##
## -------------------------------------------------- ##
import requests
import json
import argparse
import datetime
import time
import os
import re


## -------------------------------------------------- ##
## GLOBAL VAR SECTION                                 ##
## -------------------------------------------------- ##
default_ammit_report_url = 'https://acronis.ammit-web01.us-midwest-1.nexcess.net/grafana/allreport.php'
default_aged_report_url = 'https://acronis.ammit-web01.us-midwest-1.nexcess.net/grafana/dailyreport.php'


## -------------------------------------------------- ##
## CLASS SECTION                                      ##
## -------------------------------------------------- ##
class Report:

  def __init__( self,
                report_dir,
                **kwargs,):

    ## ++++++++++++++++++++ ##
    ## VARIABLE SETTING     ##
    ## ++++++++++++++++++++ ##

    ## Web URLS
    self.report_url             = default_ammit_report_url
    self.aged_report_url        = default_aged_report_url

    ## Filepaths
    self.report_dirpath         = report_dir
    self.report_filepath        = self.report_dirpath+'/error_report_list.json'
    self.aged_report_filepath   = self.report_dirpath+'/aged_report_list.json'
    self.fqdn_filter_filepath   = self.report_dirpath+'/fqdn_list.txt'

    ## Report-related vars
    self.column_lengths         = {}
    self.entire_alerts_report   = None
    self.entire_aged_report     = None
    self.alert_entries          = []
    self.aged_entries           = []
    self.no_error_hosts         = []
    self.filtered_aged_hosts    = []

    ## Filter variables
    self.filter_by                = kwargs['filter_by']
    self.use_aged_report          = kwargs.get('fqdn_file_filter',False)
    self.use_fqdn_file            = kwargs.get('use_fqdn_file',False)
    self.min_max_host_age_filter  = kwargs.get('min_max_age_filter',[1,365*9999])
    self.sort_by_column           = kwargs.get('sort_by_column','fqdn')
    self.filters                  = {}
    self.filters['display']       = set(kwargs.get('display_filter',[]))
    self.filters['date']          = kwargs.get('date_filter',time.strftime('%Y-%m-%d',time.localtime()))
    self.filters['message']       = set(kwargs.get('message_filter',[]))
    self.filters['gateway']       = set(kwargs.get('gateway_filter',[]))
    self.filters['fqdn']          = set(kwargs.get('fqdn_filter',[]))

    ## ++++++++++++++++++++ ##
    ## INITIAL PROCESS      ##
    ## ++++++++++++++++++++ ##

    ## Create min and max age filters
    self.min_host_age_filter      = self.min_max_host_age_filter[0]
    self.max_host_age_filter      = self.min_max_host_age_filter[1] \
      if len(self.min_max_host_age_filter) > 1 \
      else 365*9999

    ## Attempt to download the report (this might fail, for example, if we aren't on the VPN) 
    if kwargs.get('download_report',False):
      print('Downloading eror report')
      self.download_report(self.report_url,
                           self.report_filepath)

      print('Downloading aged report')
      self.download_report(self.aged_report_url,
                           self.aged_report_filepath)

    
    ## Load and build the report
    self.entire_alerts_report = self._load_report(self.report_filepath,)
    self.entire_aged_report   = self._load_report(self.aged_report_filepath,)
    self.__build_aged_entries_list()
    self.__build_aged_hosts()
    self.__build_alert_entries_list2()
    self._filter_report()

    ## Determine column lengths for printing/formatting
    self.__determine_column_lengths()


  def __to_days(self,
              input,):
    """
    Coming Soon
    """
  
    conversions = { 'hour'  : 0,
                    'day'   : 1,
                    'week'  : 7,
                    'month' : 30,
                    'year'  : 365,}
  
    for i in ['first','second']:
      quantity = int(input[i+'_quantity'])
      unit     = re.sub('s$','',input[i+'_quantity_unit'])
  
      return int(quantity*conversions[unit])

  def __build_aged_entries_list(self,):
    """
    Coming soon
    """

    ## {'Agent': 'jarvis02-ctl02.us-midwest-1.nexcess.net', 
    ##  'Acronis': 'ammit-us-midwest-1-gw1', 
    ##  'Last': '1 month, 3 weeks ago'}
    if self.entire_aged_report:
      for entry in self.entire_aged_report:
        if entry['Agent'] not in self.aged_entries:
          self.aged_entries.append(entry['Agent'])


  def __build_aged_hosts(self,):
    """
    Coming Soon
    """

    if not self.filtered_aged_hosts:

      if self.entire_aged_report:
        for entry in self.entire_aged_report:
          result = re.match('(?P<first_quantity>\S+) (?P<first_quantity_unit>\S+), (?P<second_quantity>\S+) (?P<second_quantity_unit>\S+) ago',entry['Last'])
          last_backup_days = self.__to_days(result.groupdict())
          if last_backup_days >= self.min_host_age_filter \
             and last_backup_days <= self.max_host_age_filter:

            self.filtered_aged_hosts.append((entry['Agent'],last_backup_days,))


  def __build_alert_entries_list2(self,):
    """
    Coming soon
    """

    ## { 'fqdn': 'cloudhost-3596980.us-midwest-2.nxcli.net', 
    ##   'Acronis': 'ammit-us-midwest-2-gw6', 
    ##   'error_msg': 'The running activity has not shown any progress for some time and may be frozen.', 
    ##   'updatedAt': '2022-03-21T08:16:44.208938667Z'}

    temporary_host_hash = {}
    for aged_fqdn,aged_fqdn_age in self.filtered_aged_hosts:
      temporary_host_hash[aged_fqdn] = {'updated'   : None,
                                        'days_aged' : aged_fqdn_age,
                                        'gateway'   : 'INT_DATA_MISSING',
                                        'message'   : 'INT_DATA_MISSING',}

    for entry in self.entire_alerts_report['alerts']:
      fqdn = entry['fqdn']
      if fqdn:
        if not temporary_host_hash.get(fqdn,None):
          temporary_host_hash[fqdn] = {'updated'    : None,
                                       'days_aged'  : 'INT_DATA_MISSING',
                                       'gateway'    : '',
                                       'message'    : '',}

        if temporary_host_hash[fqdn].get('updated',None) and \
           temporary_host_hash[fqdn]['updated'] > self._set_date(entry['updatedAt']):
          pass

        temporary_host_hash[fqdn] = {'updated'      : self._set_date(entry['updatedAt']),
                                     'days_aged'    : temporary_host_hash[fqdn]['days_aged'],
                                     'gateway'      : entry['Acronis'],
                                     'message'      : re.sub('\n','',str(entry['error_msg'])),}

    for hostname in temporary_host_hash.keys():
      self.alert_entries.append({'fqdn'    : hostname,
                                 'days_aged'     : temporary_host_hash[hostname]['days_aged'],
                                 'updated' : temporary_host_hash[hostname]['updated'],
                                 'gateway' : temporary_host_hash[hostname]['gateway'],
                                 'message' : temporary_host_hash[hostname]['message'],})

    ## print(self.alert_entries)
    ## exit()
    del(temporary_host_hash)


  def __build_alert_entries_list(self,):
    """
    Coming soon
    """

    ## { 'fqdn': 'cloudhost-3596980.us-midwest-2.nxcli.net', 
    ##   'Acronis': 'ammit-us-midwest-2-gw6', 
    ##   'error_msg': 'The running activity has not shown any progress for some time and may be frozen.', 
    ##   'updatedAt': '2022-03-21T08:16:44.208938667Z'}

    temporary_host_hash = {}
    for entry in self.entire_alerts_report['alerts']:
      fqdn = entry['fqdn']
      if fqdn:
        if not temporary_host_hash.get(fqdn,None):
          temporary_host_hash[fqdn]={}

        if temporary_host_hash[fqdn].get('updated',None) and \
           temporary_host_hash[fqdn]['updated'] > self._set_date(entry['updatedAt']):
          pass

        temporary_host_hash[fqdn] = {'updated' : self._set_date(entry['updatedAt']),
                                     'gateway' : entry['Acronis'],
                                     'message' : re.sub('\n','',str(entry['error_msg'])),}

    for hostname in temporary_host_hash.keys():
      self.alert_entries.append({'fqdn'    : hostname,
                                 'updated' : temporary_host_hash[hostname]['updated'],
                                 'gateway' : temporary_host_hash[hostname]['gateway'],
                                 'message' : temporary_host_hash[hostname]['message'],})

    del(temporary_host_hash)


  def _set_date(self, date_string):
    """
    Convert a date to a time object. Input must be in YYYY-MM-DDTHH-MM-SS for the time being
    """
    if len(date_string.split('.')) > 1:
      return_date = datetime.datetime.strptime(''.join(date_string.split('.')[0:-1]),'%Y-%m-%dT%H:%M:%S')
    elif len(date_string.split('.')) == 1:
      return_date = datetime.datetime.strptime(''.join(date_string.split('.')[0]),'%Y-%m-%dT%H:%M:%S')
    return return_date
     

  ## ================================================== ##
  ## Relevant filter methods
  def __filter_report_by_date(self,):
    """
    Filter a report by date
    """
    self.alert_entries = [entry for entry in self.alert_entries \
                          if entry['updated'] <= self._set_date(self.filters['date'])]
     

  def __filter_report_by_gateway(self,):
    """
    Filter a report by gateway(s)
    """
    self.alert_entries = [entry for entry in self.alert_entries \
                          if entry['gateway'] in self.filters['gateway']]
     

  def __filter_report_by_fqdn(self,):
    """
    Filter a report by fqdn(s)
    """
    if self.use_fqdn_file:
      if os.path.isfile(self.fqdn_filter_filepath):
        self.filters['fqdn'] = list(map(lambda x:x.strip(), open(self.fqdn_filter_filepath,'r').readlines()))

    elif self.use_aged_report:
      self.filters['fqdn'] = self.aged_entries

    self.alert_entries  = [entry for entry in self.alert_entries \
                           if entry['fqdn'] in self.filters['fqdn']]
     

  def __filter_report_by_message(self,):
    """
    Filter a report by error message(s)
    """
    tmp_alert_entries = []
    for entry in self.alert_entries:
      for message_filter in self.filters['message']:
        if re.search(message_filter,entry['message'],flags=re.IGNORECASE):
          tmp_alert_entries.append(entry)

    self.alert_entries = tmp_alert_entries
    del(tmp_alert_entries)


  def _filter_report(self,):
    """
    Coming soon
    """

    for filter_method in set(self.filter_by):
      getattr(self,'_Report__filter_report_by_'+filter_method)()

      

  ## ================================================== ##
  ## Geneneral report manipulation
  def _load_report(self,filename,):
    """
    Coming soon
    """
    return json.load(open(filename,'r'))


  def download_report(self,
                      url,
                      filepath,):
    """
    Coming soon
    """
    ## ensure that the parent path exists
    if not os.path.exists(os.path.dirname(filepath)):
      raise Exception('Please make sure that filepath {0} exists'\
                       .format(os.path.dirname(filepath)))
      
    resp = requests.get(url)


    if resp.status_code == 200:
      try:
        print('Writing report to {0}'.format(filepath,))
        open(filepath,'w').write(resp.text)
      except:
        raise Exception('Could not write to {0}'.format(filepath,))
    else:
      print('Make sure that a curl to {0} works as expected'.format(url,))
      exit(0)


  ## ================================================== ##
  ## Report output methods
  def __determine_column_lengths(self,):
    """
    Determine the length of columns
    """

    ## set the initial width of the columns to the width of the column names
    if len(self.alert_entries) >= 1:
      self.column_lengths = { k:len(k) for k in self.alert_entries[0] }

      ## iterate over the entries
      for entry in self.alert_entries:

        ## ierate over each entry column
        for column_name in entry.keys():

          ## disregard columns that don't have a value
          if entry[column_name]:

            ## create a representable column string column_string
            column_string = str(entry[column_name])

            ## check for incompatible column type instances
            if isinstance(entry[column_name],datetime.datetime):
              column_string = entry[column_name].strftime('%d-%b-%Y %H:%M:%S')
            
            if len(column_string) > self.column_lengths[column_name]:
              self.column_lengths[column_name] = len(column_string)


  def _print_header(self,):
    """
    Coming soon
    """
    self._print_border_line()
    for column_name in self.column_lengths:
      print('|',end="")
      print(' {0:{1}} '.format(column_name,self.column_lengths[column_name]),end="")
    print('|')
    self._print_border_line()


  def _print_border_line(self,):
    """
    Coming soon
    """
    for column_name in self.column_lengths:
      print('+',end="")
      print('-'*(self.column_lengths[column_name]+2),end="")
    print('+')


  def print_report_entries(self,):
    """
    Coming soon
    """

    if 'fqdn' in self.filters['display'] or \
       'all' in self.filters['display']:

      sorted_alert_entries = [ k for k in sorted(self.alert_entries,
                                                 key     = lambda x: x[self.sort_by_column],
                                                 reverse = True,)]
      if sorted_alert_entries:
        self._print_header()
        for entry in sorted_alert_entries:
          for column_name in self.column_lengths:
            content = entry[column_name]
            if isinstance(entry[column_name],datetime.datetime):
              content = entry[column_name].strftime('%d-%b-%Y %H:%M:%S')
            elif not entry[column_name] or entry[column_name] == 'INT_DATA_MISSING':
              content = '-'
            print('| {0:{1}} '.format(content,self.column_lengths[column_name]),end="")
          print('|')
        self._print_border_line()

      del(sorted_alert_entries)


  def print_counts(self, column_key):
    """
    Coming soon
    """

    if 'counts' in self.filters['display'] or \
       'all' in self.filters['display']:
      if len(self.alert_entries) >= 1:
        counts = { k:0 for k in set([ x[column_key] for x in self.alert_entries]) }

        for entry in self.alert_entries:
          if not entry[column_key] == 'INT_DATA_MISSING':
            counts[entry[column_key]] += 1

        print('\nUnique counts for {0}'.format(column_key))
        sorted_counts = sorted(counts.items(),key=lambda x:x[1],reverse=True)
        for item in sorted_counts:
          if not item[0] in 'INT_DATA_MISSING':
            print('{0:>10d} : {1:s}'.format(item[1],item[0]))

            

  def print_missing_hosts(self,):
    """
    Coming soon
    """

    if 'missing' in self.filters['display'] or \
       'all' in self.filters['display']:
      print('\n')
      alert_hosts = []
      if self.filters['fqdn']:
        alert_hosts         = [entry['fqdn'] for entry in self.alert_entries]
        ## self.no_error_hosts = [hostname for hostname in self.filters['fqdn'] \
        ##                        if hostname not in alert_hosts]

        self.no_error_hosts = set([x['fqdn'] for x in self.alert_entries \
                                   if x['gateway'] == 'INT_DATA_MISSING'])

      if len(self.no_error_hosts) > 0:
        print('FQDNs without alert entries ({0} hosts)'\
              .format(len(self.no_error_hosts)))
        for fqdn_name in sorted(self.no_error_hosts):
          print(' '*10+' - {0}'.format(fqdn_name))

      del(alert_hosts)



  def print_duplicate_hosts(self,):
    """
    Coming soon
    """
    
    if 'duplicates' in self.filters['display'] or \
       'all' in self.filters['display']:

      ## self.__build_aged_hosts()

      duplicates = { k:0 for k in map(lambda x:x[0], self.filtered_aged_hosts)}
      for i in map(lambda x:x[0], self.filtered_aged_hosts):
        duplicates[i] += 1

      duplicates = {k:duplicates[k] for k in duplicates.keys() if duplicates[k] > 1}

      print('\nDuplicate aged FQDNs ({0} duplicate hosts)'.format(len(duplicates)))
      for hostname,count in sorted(duplicates.items(), key=lambda x:x[1], reverse=True):
        print('{0:>10d} : {1:s}'.format(count,hostname))
      ## print(self.filtered_aged_hosts)
    



  def print_aged_hosts(self,):
    """
    Coming soon
    """
    if 'aged' in self.filters['display'] or \
       'all' in self.filters['display']:

      print('\nAged hosts with reported backup between {0} and {1} days ({2} hosts)'\
            .format(self.min_host_age_filter,
                    self.max_host_age_filter,
                    len(self.entire_aged_report),))

      for aged_host_tuple in sorted(self.filtered_aged_hosts,
                                     key     = lambda entry: entry[1],
                                     reverse = True,):
        print('{0:>10d} : {1:s}'.format(aged_host_tuple[1],
                                      aged_host_tuple[0],))




## -------------------------------------------------- ##
## MAIN BODY                                          ##
## -------------------------------------------------- ##
if __name__ == '__main__':

  a = argparse.ArgumentParser()


  ## Filter arguments
  a.add_argument( '-f',
                  '--filter-by',
                  dest    = 'filter_by',
                  action  = 'store',
                  choices = [ 'date',
                              'fqdn',
                              'gateway',
                              'message',],
                  help    = 'Determine how to filter the report',
                  nargs   = '+',
                  default = ['fqdn',],)

  a.add_argument( '-s',
                  '--sort-by',
                  dest    = 'sort_by_column',
                  action  = 'store',
                  choices = [ 'days_aged',
                              'date',
                              'fqdn',
                              'gateway',
                              'message',],
                  help    = 'Determine how to sort FQDN-based report',
                  default = 'fqdn',)

  a.add_argument( '-Fs',
                  '--display',
                  dest    = 'display_filter',
                  action  = 'store',
                  choices = [ 'aged',
                              'all',
                              'counts',
                              'duplicates',
                              'fqdn',
                              'missing', ],
                  help    = 'Dictate whether end report has counts, per-fqdn entries, or both',
                  nargs   = '+',
                  default = [''],)

  a.add_argument( '-Fd',
                  '--date',
                  dest    = 'date_filter',
                  action  = 'store',
                  help    = 'Date to use for filtering (by date). Format is YYYY-MM-DDTHH:MM:SS',)

  a.add_argument( '-Fm',
                  '--message',
                  dest    = 'message_filter',
                  action  = 'append',
                  help    = 'Error message to use for filtering',
                  default = [],)

  a.add_argument( '--min-max-age-filter',
                  dest    = 'min_max_age_filter',
                  action  = 'extend',
                  type    = int,
                  help    = 'Minimum (and optionally maximum) age, in days, threshold for a host',
                  nargs   = '+',
                  default = [1,365*9999],)

  a.add_argument( '-Fg',
                  '--gateway',
                  dest    = 'gateway_filter',
                  action  = 'append',
                  help    = 'Gateway to use for filtering',
                  default = [],)

  a.add_argument( '-Ff',
                  '--fqdn-name',
                  dest    = 'fqdn_filter',
                  action  = 'append',
                  help    = 'FQDN(s) to use for filtering',
                  default = [],)

  a.add_argument( '-Fa',
                  '--use-fqdn-report',
                  dest    = 'fqdn_file_filter',
                  action  = 'store_true',
                  help    = 'Used aged report FQDN list for filtering',)

  ## Arguments related to the alerts report and its location
  a.add_argument( '-l',
                  '--report-location',
                  dest    = 'report_dir',
                  action  = 'store',
                  help    = 'Directory used for the downloaded reports',
                  default = '/tmp/ammit_report_today',)

  a.add_argument( '-d',
                  '--download',
                  '--download-report',
                  dest    = 'download_report',
                  action  = 'store_true',
                  help    = 'Flag to download report', )

  a.add_argument( '--use-fqdn-file',
                  dest    = 'use_fqdn_file',
                  action  = 'store_true',
                  help    = 'Use a fqdn file with a list of files (should be called fqdn_list.txt)',)


  opts      = a.parse_args()
  opts_dict = vars(opts)

  ## make sure that we include fqdn filterif we use fqdn report or file
  if opts_dict['use_fqdn_file'] or \
     opts_dict['fqdn_file_filter']:

    opts_dict['filter_by'].append('fqdn')

  ## create a Report object
  report = Report(**opts_dict)

  print('\n\n')
  report.print_report_entries()
  report.print_counts('gateway')
  report.print_counts('message')
  report.print_aged_hosts()
  report.print_duplicate_hosts()
  report.print_missing_hosts()
             


## vim:syntax=python:ai:et:ts=2:sw=2: